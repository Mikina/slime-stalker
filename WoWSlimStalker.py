import json
import urllib.parse

import requests
import re
import pprint
from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session

#You should get those for registering at https://develop.battle.net/access/clients
client_id = 'CLIENT_ID_HERE'
client_secret = 'CLIENT_SECRET_HERE'

client = BackendApplicationClient(client_id=client_id)
oauth = OAuth2Session(client=client)
global token
token = oauth.fetch_token(token_url='https://eu.battle.net/oauth/token', client_id=client_id,
        client_secret=client_secret)


def GetCharacterMounts(character, realm =False, refreshed=False):
    global token
    headers = {"Authorization":"Bearer "+token["access_token"]}
    response = requests.get("https://eu.api.blizzard.com/profile/wow/character/"+character.realm+"/"+character.name.lower()+"/collections/mounts?namespace=profile-eu&locale=en_GB", headers=headers)
    print(str(response.status_code)+ " | " + character.name + "-" + character.realm)
    if(response.status_code == 401 and not refreshed):
        token = oauth.fetch_token(token_url='https://eu.battle.net/oauth/token', client_id=client_id,
                                  client_secret=client_secret)
        return GetCharacterMounts(character,realm,True)

    if(response.status_code == 404 and not realm):
        character.realm = getRealmStup(character.realm)
        return GetCharacterMounts(character,True,refreshed)

    if(response.status_code != 200):
        pprint.pprint(response.text)
        return False
    data = json.loads(response.text)
    mountString = ""
    for mount in data["mounts"]:
        mountString +=mount["mount"]["name"]+","
        if "slime" in mount["mount"]["name"].lower():
            print("!!!!!!Found mount!!!!!")
            print(character.name + " on " + character.realm)
            return True
    print(mountString)
    return False
    #pprint.pprint(data)

class Character:
    def __init__(self, name, realm):
        name = urllib.parse.quote(urllib.parse.unquote_plus(name).lower())
        self.name = name
        self.realm = realm

    def __str__(self):
        return self.name + "-" + self.realm

    def __repr__(self):
        return self.name + "-" + self.realm

def parsePageNumber(number):
    print("---- PAGE "+str(number)+ "------")
    response = requests.post("https://www.wowprogress.com/gearscore/eu/char_rating/next/"+str(number),{"ajax" : "1"})
    parsedChars = []
    allChars = re.findall(r'href="/character/eu/[^"]*', response.text)
    for char in allChars:
        tmp = char.split("/")
        parsedChars.append(Character(tmp[4],tmp[3]))
    return parsedChars

def getRealmStup(realmName):
    print("Unknown realm:" + urllib.parse.unquote(realmName))
    headers = {"Authorization":"Bearer "+token["access_token"]}
    resp = requests.get("https://eu.api.blizzard.com/data/wow/realm/index?namespace=dynamic-eu", headers=headers)
    data = json.loads(resp.text)
    #pprint.pprint(data)
    for realm in data["realms"]:
        for name in realm["name"].values():
            stub = name.lower().replace(" ", "-")
            stub = stub.replace("'","-")
            #print(stub)
            if urllib.parse.unquote(realmName) in stub:
                print("Found Slug:" + realm["slug"])
                return realm["slug"]
    return "Unknown"
firstError = ""
lastError = ""
for i in range(952,2250):
    try:
        chars = parsePageNumber(i)
        for char in chars:
            if(GetCharacterMounts(char)):
                input("!!!!!!!")
    except Exception as e:
        if firstError == "":
            firstError = str(i)
        lastError = str(i)
        print("ERROR: First: "+firstError + " Last: "+ lastError)
        pprint.pprint(e)


for i in range(0,396):
    try:
        chars = parsePageNumber(i)
        for char in chars:
            if(GetCharacterMounts(char)):
                input("!!!!!!!")
    except Exception as e:
        if firstError == "":
            firstError = str(i)
        lastError = str(i)
        print("ERROR: First: "+firstError + " Last: "+ lastError)
        pprint.pprint(e)


#print(getRealmStup("%D1%80%D0%B5%D0%B2%D1%83%D1%89%D0%B8%D0%B9-%D1%84%D1%8C%D0%BE%D1%80%D0%B4"))
#resp = requests.get("https://eu.api.blizzard.com/data/wow/search/connected-realm?namespace=dynamic-eu&realms.name=", headers=headers)
