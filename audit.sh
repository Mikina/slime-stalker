#!/bin/bash

# Audit Tomcat9/Nginx
# Author: Filip Zvaric, Mikulas Hrdlicka


##################################################################

VERSION="Audit Webserver Tomcat";



############### Globals ###############

## Expression used by egrep to ignore comments
## egrep is not as powerful as perl, and differs a bit between platforms.
comments='^#|^ +#|^$|^ $';
#comments='^#|^    #|^$|^ $';
#comments='^#|^$|^ +$';
# f=$0.$$.out

## Output results to screen also?
## These actually have no meaning, I must clean them up.
## in fact all output is to stdout or stderr.
# VERBOSE='1';
#VERBOSE_SUM='1';
# FILE='0';

############### Setup ###############

###*Setup*
## DESTDIR: set to define an alternate output dir, default is `fldr`
foldername=tomcat-`date "+%F-%H%M"`
fldr=audit
DESTDIR="$fldr/$foldername"

##Record the start time to show it at the end
start_time=`date`
countervar=1

## Create a dir with the following name tomcat-hostname-date
echo "Creating $DESTDIR to store all the results"
mkdir -p $DESTDIR


## Save stdout and err just in case
# exec 5>&1
# exec 6>&2

## Redirect stdout and stderr to logfiles in the directory
logfile="$DESTDIR/tomcat-basic.log"
#errlogfile="$DESTDIR/errors.log"
#echo "To followup on progress: tail -f $logfile $errlogfile"
#exec > "$logfile" 2> "$errlogfile"
# echo "To followup on progress: tail -f $logfile"
# Close STDOUT file descriptor
exec 3>&1
exec 1<&-
# Close STDERR FD
exec 2<&-
# Open STDOUT as $LOG_FILE file for read and write.
exec 1<>"$logfile"
# Redirect STDERR to STDOUT
exec 2>&1


#exec 3>&1 >"$logfile" 2>>"$logfile"


## OS name, version and hardware
os=`uname -s`
## We check for SunOS, Linux, HP-UX and OpenBSD
rel=`uname -r`
hw=`uname -m`


if [ "$os" = "Linux" ] ; then
  #echo $os;
  echo='/bin/echo -e'
  #ps='ps -auwx';
  ps='ps auwx';
  proc1='/bin/ps -e -o comm ';
  fstab='/etc/fstab';
  shares='/etc/exports';
  lsof='lsof -i';
  mount='mount';
  PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/X11R6/bin:/usr/lib/saint/bin:/opt/postfix:/usr/lib/java/jre/bin
  aliases='/etc/aliases';
  key_progs='/usr/bin/passwd /bin/login /bin/ps /bin/netstat';
  snmp='/etc/snmp/snmpd.conf';
  crontab='crontab -u root -l';
  ntp='/etc/ntp.conf';
fi

## common programs
sum='/usr/bin/sum';

############### Functions ###############
check_err () {
  if [ "$*" != "0" ] ; then
    $echo "SCRIPT $0 ABORTED: error." >>$f 2>&1
    send_results;
    exit 1;
  fi
}
run () {
  if [ $FILE = "1" ]    ; then $echo "Running command: $*" >>$f;   fi
  if [ $VERBOSE = "1" ] ; then $echo "Running command: $*";        fi
  if [ $FILE = "1" ]    ; then $*    >> $f;
  else
    $*;
  fi
}

counter () {
	#lastline=$(tail -c 1 $logfile)
	#if [ "$lastline" != "" ]; then
	#	echo ""
	#fi
	echo "-AUDIT CHECK ${countervar}:"
	countervar=$((countervar+1))
}


############### AUDIT ###############

echo "---------- AUDIT ----------"
echo " "
date
echo " "

# What OS release are we running?
run uname -a
echo " "
$echo "Logged on users:"
w
echo " "

# Privilege check
[ `whoami` = root ] || echo "Please run the script with sudo." 1>&3
[ `whoami` = root ] || exit

echo " "


############### APACHE TOMCAT 9 AUDIT ###############
echo "Apache Tomcat 9 Cis Benchmark v1.1.0"
echo ""
##Backup HOME and BASE variable values.
OLD_HOME=$CATALINA_HOME
OLD_BASE=$CATALINA_BASE
input=y
# Only set CATALINA_HOME if not already set
[ -z "$CATALINA_HOME" ] && CATALINA_HOME=`cd "$(find / -name catalina.sh -print -quit 2>/dev/null | xargs -I{} dirname {})/.." >/dev/null;pwd` && echo -n "CATALINA_HOME not set. Is $CATALINA_HOME correct path? (y/N): " 1>&3 && read input 

case $input in
     [yY][eE][sS]|[yY])
 break
 ;;
     [nN][oO]|[nN])
 echo "Please set CATALINA_BASE and CATALINA_HOME env variable to the correct value and run the script again." 1>&3
 exit
        ;;
     *)
 echo "Please set CATALINA_BASE and CATALINA_HOME env variable to the correct value and run the script again." 1>&3
 exit
 ;;
 esac

input=y

# Copy CATALINA_BASE from CATALINA_HOME if not already set
[ -z "$CATALINA_BASE" ] && CATALINA_BASE="$CATALINA_HOME"&&  echo -n "CATALINA_BASE not set. Is $CATALINA_BASE correct path? (y/N): " 1>&3 && read input

case $input in
     [yY][eE][sS]|[yY])
 break
 ;;
     [nN][oO]|[nN])
 echo "Please set CATALINA_BASE and CATALINA_HOME variable to the correct value and run the script again." >/dev/stderr
 exit
        ;;
     *)
 echo "Please set CATALINA_BASE and CATALINA_HOME variable to the correct value and run the script again." >/dev/stderr
 exit
 ;;
 esac



audit_tomcat() {
countervar=1
#1 Remove Extraneous Resources
##1.1 Remove extraneous files and directories
counter
ls -l $CATALINA_HOME/webapps/examples \
	  $CATALINA_HOME/webapps/docs \
	  $CATALINA_HOME/webapps/ROOT \
	  $CATALINA_HOME/webapps/host-manager \
	  $CATALINA_HOME/webapps/manager


##1.2 Disable Unused Connectors
counter
grep “Connector” $CATALINA_HOME/conf/server.xml
echo "---BASE---"
grep “Connector” $CATALINA_BASE/conf/server.xml


#2 Limit Server Platform Information Leaks
##2.1 Alter the Advertised server.info String
counter
cd $CATALINA_HOME/lib
jar xf catalina.jar org/apache/catalina/util/ServerInfo.properties
grep server.info org/apache/catalina/util/ServerInfo.properties



##2.2 Alter the Advertised server.number String
counter
cd $CATALINA_HOME/lib
jar xf catalina.jar org/apache/catalina/util/ServerInfo.properties
grep server.number org/apache/catalina/util/ServerInfo.properties


	  
##2.3 Alter the Advertised server.built Date
counter
cd $CATALINA_HOME/lib
jar xf catalina.jar org/apache/catalina/util/ServerInfo.properties
grep server.built org/apache/catalina/util/ServerInfo.properties



##2.4 Disable X-Powered-By HTTP Header and Rename the Server Value
##2.7 Ensure Sever Header is Modified To Prevent Information Disclosure
counter
echo "---HOME conf server.xml----"
cat $CATALINA_HOME/conf/server.xml
echo "---BASE conf server.xml----"
cat $CATALINA_BASE/conf/server.xml

##2.5 Disable client facing Stack Traces (Automated)
##2.6 Turn off TRACE
#2.7 Ensure Sever Header is Modified To Prevent Information Disclosure
counter
echo "---HOME conf web.xml----"
echo ""
cat $CATALINA_HOME/conf/web.xml
echo "---BASE conf web.xml----"
echo ""
cat $CATALINA_BASE/conf/web.xml
echo ""
for f in "$CATALINA_HOME"/webapps/*/WEBINF/web.xml; do echo "";echo "";echo "-------File $f ------";echo ""; cat "$f"; done
for f in "$CATALINA_BASE"/webapps/*/WEBINF/web.xml; do echo "";echo "";echo "-------File $f ------";echo ""; cat "$f"; done


#3 Protect the Shutdown Port
##3.1 Set a nondeterministic Shutdown command value
counter
cd $CATALINA_HOME/conf
grep 'shutdown[[:space:]]*=[[:space:]]*"SHUTDOWN"' server.xml

##3.2 Disable the Shutdown port
counter
cd $CATALINA_HOME/conf/
grep '<Server[[:space:]]\+[^>]*port[[:space:]]*=[[:space:]]*"-1"' server.xml


#4 Protect Tomcat Configurations
##4.1 Restrict access to $CATALINA_HOME (Automated)
counter
cd $CATALINA_HOME
find . -follow -maxdepth 0 \( -perm /o+rwx,g=w -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.2 Restrict access to $CATALINA_BASE
counter
cd $CATALINA_BASE
find . -follow -maxdepth 0 \( -perm /o+rwx,g=w -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.3 Restrict access to Tomcat configuration directory (Automated)
counter
cd $CATALINA_HOME/conf
find . -maxdepth 0 \( -perm /o+rwx,g=w -o ! -user tomcat_admin -o ! -group tomcat \) -ls
echo "---BASE---"
cd $CATALINA_BASE/conf
find . -maxdepth 0 \( -perm /o+rwx,g=w -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.4 Restrict access to Tomcat logs directory
counter
cd $CATALINA_HOME
find logs -follow -maxdepth 0 \( -perm /o+rwx -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.5 Restrict access to Tomcat temp directory (Automated)
counter
cd $CATALINA_HOME
find temp -follow -maxdepth 0 \( -perm /o+rwx -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.6 Restrict access to Tomcat binaries directory (Automated)
counter
cd $CATALINA_HOME
find bin -follow -maxdepth 0 \( -perm /o+rwx,g=w -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.7 Restrict access to Tomcat web application directory
counter
cd $CATALINA_HOME
find webapps -follow -maxdepth 0 \( -perm /o+rwx,g=w -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.8 Restrict access to Tomcat catalina.properties (Automated)
counter
cd $CATALINA_HOME/conf/
find catalina.properties -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+x -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.9 Restrict access to Tomcat catalina.policy (Automated)
counter
cd $CATALINA_HOME/conf/
find catalina.policy -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+x -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.10 Restrict access to Tomcat context.xml (Automated)
counter
cd $CATALINA_HOME/conf
find context.xml -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+x -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.11 Restrict access to Tomcat logging.properties (Automated)
counter
cd $CATALINA_HOME/conf/
find logging.properties -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+x -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.12 Restrict access to Tomcat server.xml (Automated)
counter
cd $CATALINA_HOME/conf/
find server.xml -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+x -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.13 Restrict access to Tomcat tomcat-users.xml (Automated)
counter
cd $CATALINA_HOME/conf/
find tomcat-users.xml -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+x -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.14 Restrict access to Tomcat web.xml (Automated)
counter
cd $CATALINA_HOME/conf/
find web.xml -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+wx -o ! -user tomcat_admin -o ! -group tomcat \) -ls

##4.15 Restrict access to jaspic-providers.xml (Automated)
counter
cd $CATALINA_HOME/conf/
find jaspic-providers.xml -follow -maxdepth 0 \( -perm /o+rwx,g+rwx,u+x -o ! -user tomcat_admin -o ! -group tomcat \) -ls


#5 Configure Realms
##5.1 Use secure Realms (Automated)
counter
grep "Realm className" $CATALINA_HOME/conf/server.xml | grep MemoryRealm
grep "Realm className" $CATALINA_HOME/conf/server.xml | grep JDBCRealm
grep "Realm className" $CATALINA_HOME/conf/server.xml | grep UserDatabaseRealm
grep "Realm className" $CATALINA_HOME/conf/server.xml | grep JAASRealm

##5.2 Use LockOut Realms (Automated)
counter
grep "LockOutRealm" $CATALINA_HOME/conf/server.xml


#6 Connector Security
##6.1 Setup Client-cert Authentication (Automated)
#Review the Connector configuration in server.xml and ensure the clientAuth is set to true and certificateVerification is set to required.
counter
cat $CATALINA_HOME/conf/server.xml | grep clientAuth
cat $CATALINA_HOME/conf/server.xml | grep certificateVerification

##6.2 Ensure SSLEnabled is set to True for Sensitive Connectors(Automated)
counter
cat $CATALINA_HOME/conf/server.xml | grep SSLEnabled

##6.3 Ensure scheme is set accurately (Automated)
counter
cat $CATALINA_HOME/conf/server.xml | grep scheme

##6.4 Ensure secure is set to true only for SSL-enabled Connectors
counter
cat $CATALINA_HOME/conf/server.xml | grep "Connector\|SSLEnabled\|secure="

##6.5 Ensure 'sslProtocol' is Configured Correctly for Secure Connectors
counter
cat $CATALINA_HOME/conf/server.xml | grep "Connector\|SSLEnabled\|sslProtocol"


#7 Establish and Protect Logging Facilities
##7.1 Application specific logging (Automated)
counter
for f in "$CATALINA_BASE"/webapps/*/WEB-INF/classes; do echo "";echo "";echo "-------Folder $f ------"; ls "$f"; done

##7.2 Specify file handler in logging.properties files (Automated)
counter
for f in "$CATALINA_BASE"/webapps/*/WEB-INF/classes/logging.properties; do echo "";echo "";echo "-------File $f ------"; grep handlers "$f"; done
echo "";
echo "";
echo "----- Base folder ----"
grep handlers "$CATALINA_BASE"/conf/logging.properties

##7.3 Ensure className is set correctly in context.xml (Automated)
counter
for f in "$CATALINA_BASE"/webapps/*/META-INF/context.xml; do echo "";echo "";echo "-------File $f ------"; grep org.apache.catalina.valves.AccessLogValve "$f"; done

##7.4 Ensure directory in context.xml is a secure location (Automated)
counter
for f in "$CATALINA_BASE"/webapps/*/META-INF/context.xml; do echo "";echo "";echo "-------File $f ------"; logDir=$(grep directory "$f" | grep -e "\"[^\"]*\"" -o); eval echo $logDir; ls -ld $(eval echo $logDir); done

##7.5 Ensure pattern in context.xml is correct (Automated)
counter
for f in "$CATALINA_BASE"/webapps/*/META-INF/context.xml; do echo "";echo "";echo "-------File $f ------"; grep pattern "$f"; done

##7.6 Ensure directory in logging.properties is a secure location
counter
for f in "$CATALINA_BASE"/webapps/*/WEBINF/classes/logging.properties; do echo "";echo "";echo "-------File $f ------"; logDir=$(grep directory "$f" | grep -e "\"[^\"]*\"" -o); eval echo $logDir; ls -ld $(eval echo $logDir); done
echo ""
echo ""
echo "------ File conf/logging.properties-----"
logDir=$(grep directory "$CATALINA_BASE/conf/logging.properties" | grep -e "\"[^\"]*\"" -o)
eval echo $logDir
ls -ld $(eval echo $logDir);

#8 Configure Catalina Policy
##8.1 Restrict runtime access to sensitive packages (Automated)
counter
echo "---catalina.properties-----"
cat $CATALINA_BASE/conf/catalina.properties

##9.3 Disable deploy on startup of applications (Automated)
counter
grep "deployOnStartup" $CATALINA_HOME/conf/server.xml



#9 Application Deployment
##9.1 Starting Tomcat with Security Manager (Manual)
counter
ls /etc/init.d
for f in /etc/init.d/*tomcat*; do echo "";echo "";echo "-------File $f ------"; cat "$f"; done
for f in /etc/init.d/*catalina*; do echo "";echo "";echo "-------File $f ------"; cat "$f"; done

##9.2 Disabling auto deployment of applications (Automated)
counter
grep "autoDeploy" $CATALINA_HOME/conf/server.xml


#10 Miscellaneous Configuration Settings
##10.1 Ensure Web content directory is on a separate partition from the Tomcat system files (Manual)
counter
df $CATALINA_HOME/webapps
echo "---"
df $CATALINA_HOME

##10.2 Restrict access to the web administration application (Automated)
counter
grep -B 4 -A 4 RemoteAddrValve $CATALINA_HOME/conf/server.xml

##10.3 Restrict manager application (Manual)
counter
for f in "$CATALINA_BASE"/conf/*/*/manager.xml; do echo "";echo "";echo "-------File $f ------"; cat "$f"; done

echo ""
echo ""
echo "--- GREP ---"
for f in "$CATALINA_BASE"/conf/*/*/manager.xml; do echo "";echo "";echo "-------File $f ------"; grep -B 4 -A 4 RemoteAddrValve "$f"; done

##10.4 Force SSL when accessing the manager application (Manual)
counter
grep transport-guarantee $CATALINA_HOME/webapps/manager/WEB-INF/web.xml

##10.5 Rename the manager application (Manual)
counter
file $CATALINA_HOME/conf/Catalina/localhost/manager.xml
file $CATALINA_HOME/webapps/host-manager/manager.xml
file $CATALINA_HOME/webapps/manager

##10.6 Enable strict servlet Compliance (Manual)
counter
echo "----catalina.sh file----"
cat $CATALINA_HOME/bin/catalina.sh
echo "-----"
grep STRICT_SERVLET_COMPLIANCE $CATALINA_HOME/bin/catalina.sh
##10.7 Turn off session facade recycling (Manual)
counter
grep RECYCLE_FACADES $CATALINA_HOME/bin/catalina.sh


##10.8 Do not allow additional path delimiters (Manual)
counter
grep ALLOW_BACKSLASH $CATALINA_HOME/bin/catalina.sh
grep ALLOW_ENCODED_SLASH $CATALINA_HOME/bin/catalina.sh

##10.9 Configure connectionTimeout (Automated)
counter
grep connectionTimeout $CATALINA_HOME/conf/server.xml

##10.10 Configure maxHttpHeaderSize (Automated)
counter
grep maxHttpHeaderSize $CATALINA_HOME/conf/server.xml

##10.11 Force SSL for all applications (Automated)
counter
grep transport-guarantee $CATALINA_HOME/conf/web.xml

##10.12 Do not allow symbolic linking (Automated)
counter
cd $CATALINA_HOME
find . -name context.xml | xargs grep "allowLinking"

##10.13 Do not run applications as privileged (Automated)
counter
find . -name context.xml | xargs grep "privileged"

##10.14 Do not allow cross context requests (Automated)
counter
find . -name context.xml | xargs grep "crossContext"

##10.15 Do not resolve hosts on logging valves (Automated)
counter
grep enableLookups $CATALINA_HOME/conf/server.xml

##10.16 Enable memory leak listener (Automated)
counter
grep -B 4 -A 4 JreMemoryLeakPreventionListener $CATALINA_HOME/conf/server.xml

##10.17 Setting Security Lifecycle Listener (Automated)
counter
grep -B 4 -A 8 SecurityListener $CATALINA_HOME/conf/server.xml

##10.18 Use the logEffectiveWebXml and metadata-complete settings for deploying applications in production (Automated)
counter
for f in "$CATALINA_HOME"/webapps/*/WEBINF/web.xml; do echo "";echo "";echo "-------File $f ------";echo ""; grep -B 2 -A 2 metadata-complete "$f"; done
for f in "$CATALINA_HOME"/webapps/*/META-INF/context.xml; do echo "";echo "";echo "-------File $f ------";echo ""; grep -B 2 -A 2 logEffectiveWebXml "$f"; done

##10.19 Ensure Manager Application Passwords are Encrypted (Manual)
counter
grep -i <login-config>[.\n]*<auth-method>DIGEST</auth-method>[.\n]*<realm-name>UserDatabase</realm-name>[.\n]*</login-config> $CATALINA_HOME/webapps/manager/WEB-INF/web.xml

echo ""
echo "-------------------------"
echo "---TOMCAT AUDIT DONE-----"
echo "-------------------------"
echo ""
echo ""

}
echo "----STARTING TOMCAT AUDIT-----" 1>&3
echo "----TOMCAT AUDIT DEFAULT ENV VALUES----"
echo "BASE=$CATALINA_BASE"
echo "HOME=$CATALINA_HOME"
audit_tomcat

echo "----TOMCAT AUDIT SWAPPED ENV VALUES----"
TMP=$CATALINA_HOME
CATALINA_HOME=$CATALINA_BASE
CATALINA_BASE=$TMP
echo "BASE=$CATALINA_BASE"
echo "HOME=$CATALINA_HOME"
audit_tomcat


CATALINA_HOME=$(ps -aux | grep -e "catalina\.home=[^ ]*" -o | grep -e "[^=]*$" -o)
CATALINA_BASE=$(ps -aux | grep -e "catalina\.base=[^ ]*" -o | grep -e "[^=]*$" -o)

echo "----TOMCAT AUDIT PARSED ENV VALUES FROM PS AUX----"
echo "BASE=$CATALINA_BASE"
echo "HOME=$CATALINA_HOME"
audit_tomcat

CATALINA_HOME=`cd "$(find / -name catalina.sh -print -quit 2>/dev/null | xargs -I{} dirname {})/.." >/dev/null;pwd`
CATALINA_BASE="$CATALINA_HOME"
echo "----TOMCAT AUDIT PARSED ENV VALUES FROM catalina.sh location----"
echo "BASE=$CATALINA_BASE"
echo "HOME=$CATALINA_HOME"
audit_tomcat

#Resume old HOME and BASE variable values.
CATALINA_BASE=OLD_BASE
CATALINA_HOME=OLD_HOME

echo "----TOMCAT AUDIT DONE-----" 1>&3



$echo "\n\n"
$echo "Start time:	$start_time"
end_time=`date`
$echo "End time:	$end_time"
$echo ">>>>>>>>>> Done <<<<<<<<<<<"
echo "----Results saved in $logfile----" 1>&3

# exec >&5 2>&6

##Use $results_file to store the file name
##Create a Tar of the folder
#if [ "$os" = "Linux" ]; then
#	tar zcf $foldername.tar.gz $foldername
#	reportfilename="$foldername.tar.gz"

#elif [ -f /usr/bin/gzip ]; then
#	tar cf $foldername.tar $foldername
#	/usr/bin/gzip $foldername.tar
#	reportfilename="$foldername.tar.gz"

#else
#	tar cf $foldername.tar $foldername
#	compress $foldername.tar
#	reportfilename="$foldername.tar.Z"
	
#fi

## Send it by email
##Todo: send email - find a better way to do this
##  uuencode for Solaris is /bin/uuencode and in Linux /usr/bin/uuencode, so lets test for both before sending the email
#if [ $DESTEMAIL  ]; then 
#	if [ -f /usr/bin/uuencode ] || [ -f /bin/uuencode ] ; then 
#		uuencode  $reportfilename $reportfilename | mail -s "$foldername" $DESTEMAIL 
#		$echo "An email has been sent to $DESTEMAIL with the output"
#		email_sent=1
#	else
#		$echo "The report couldn't be attached because uuencode is not installed on this server" | mail -s "$foldername file not attached" $DESTEMAIL
#		$echo "\n\nThe report couldn't be emailed because uuencode is not installed on this server! \n\n"
#	fi
#
#fi

##Leave files on disk in either a tar.gz or a directory
#if [ $LEAVE ]; then 
#	$echo "Results were saved in $DESTDIR"
#	rm -f $reportfilename
#else
	##Delete the result directory
#	$echo "Results were saved in $fldr/$reportfilename"
#	rm -rf $DESTDIR
#fi

##Cleanup
#if [ $CLEANUP ]; then 
#	$echo "Resulting files will be removed"
#	rm -rf $DESTDIR
#	rm -f $reportfilename
#fi











# 
# 
# 
# 
# # 
# # # 
# # # # 
# # # # # 
# # # # # # 
# # # # # # # 
# # # # # # # # 
# # # # # # # # # 